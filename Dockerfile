FROM python:3.12.1 AS builder

COPY . .

RUN pip install --user -r requirements.txt && \
    python3 setup.py build && \
    python3 setup.py install --user

FROM python:3.12.1-slim

RUN adduser --uid 1000 --disabled-password --gecos '' kubitus-ingress-operator

COPY --from=builder --chown=kubitus-ingress-operator:kubitus-ingress-operator /root/.local /home/kubitus-ingress-operator/.local

USER 1000

ENV PATH=/home/kubitus-ingress-operator/.local/bin:$PATH

ENTRYPOINT ["kubitus-ingress-operator"]
