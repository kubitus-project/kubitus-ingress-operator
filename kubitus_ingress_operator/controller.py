# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Mathieu Parent <math.parent@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import annotations

import json
import logging
from http.server import BaseHTTPRequestHandler
from typing import Any

from kubitus_ingress_operator.ingress2certificate import Ingress2Certificate
from kubitus_ingress_operator.ingress2istio import Ingress2Istio


STATUS_OK = 200
STATUS_NOT_FOUND = 404

logger = logging.getLogger(__name__)


class Controller(BaseHTTPRequestHandler):
    """Base HTTP request handler."""

    def send_not_found(self) -> None:
        """Send 404 Not Found response."""
        self.send_response(STATUS_NOT_FOUND)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        self.wfile.write('{"message":"404 Not Found"}'.encode())

    def send_ok(self, body: Any) -> None:
        """Send 200 OK response.

        Args:
            body: Body, JSON-encoded before sending.
        """
        self.send_response(STATUS_OK)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        logger.debug('Sending: %s', json.dumps(body))
        self.wfile.write(json.dumps(body).encode())

    def do_GET(self) -> None:  # noqa: N802
        """Handle GET requests."""
        if self.path == '/healthz':
            self.send_ok({'message': 'OK'})
            return
        self.send_not_found()

    def do_POST(self) -> None:  # noqa: N802
        """Handle POST requests."""
        request = self._json_body()
        logger.debug('Received %s on %s: %s', 'POST', self.path, json.dumps(request))
        if self.path == '/ingress2certificate/customize':
            self.send_ok(Ingress2Certificate().customize(request['parent']))
            return
        elif self.path == '/ingress2certificate/sync':
            self.send_ok(Ingress2Certificate().sync(
                request['parent'],
                request['children'],
                request['related'],
            ))
            return
        elif self.path == '/ingress2istio/customize':
            self.send_ok(Ingress2Istio().customize(request['parent']))
            return
        elif self.path == '/ingress2istio/sync':
            self.send_ok(Ingress2Istio().sync(
                request['parent'],
                request['children'],
                request['related'],
            ))
            return
        self.send_not_found()

    def _json_body(self) -> Any:
        content_length = int(self.headers.get('content-length'))  # type: ignore
        request_body = self.rfile.read(content_length)
        return json.loads(request_body)
