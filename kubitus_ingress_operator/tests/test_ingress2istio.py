# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Mathieu Parent <math.parent@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from unittest import TestCase

from kubitus_ingress_operator.ingress2istio import Ingress2Istio
from kubitus_ingress_operator.tests.fixtures import ingress, ingress_class


class TestIngress2Istio(TestCase):
    """Test Ingress2Istio class."""

    def test_customize(self) -> None:
        """Test customize method."""
        parent = ingress(
            'rook-ceph-mgr-dashboard',
            'rook-ceph',
            'rook-ceph.example.org',
            'rook-ceph-mgr-dashboard',
            'http-dashboard',
            annotations={'cert-manager.io/cluster-issuer': 'ca-issuer'},
        )
        actual = Ingress2Istio().customize(parent)
        expected = {
            'relatedResources': [
                {
                    'apiVersion': 'networking.k8s.io/v1',
                    'resource': 'ingressclasses',
                },
                {
                    'apiVersion': 'v1',
                    'resource': 'services',
                    'namespace': 'rook-ceph',
                    'names': ['rook-ceph-mgr-dashboard'],
                },
            ],
        }
        self.assertEqual(actual, expected)

    def test_sync_default_ingress_class(self) -> None:
        """Test sync method; with default ingress class."""
        parent = ingress(
            'rook-ceph-mgr-dashboard',
            'rook-ceph',
            'rook-ceph.example.org',
            'rook-ceph-mgr-dashboard',
            'http-dashboard',
            annotations={'cert-manager.io/cluster-issuer': 'ca-issuer'},
        )

        children: list = []
        related = {
            'IngressClass.networking.k8s.io/v1': {
                'kubitus-ingress-operator': ingress_class(
                    'kubitus-ingress-operator',
                    annotations={
                        'ingressclass.kubernetes.io/is-default-class': 'true',
                    },
                ),
            },
            'Service.v1': {
                'rook-ceph-mgr-dashboard': {
                    'spec': {
                        'ports': [{'name': 'http-dashboard', 'port': 42}],
                    },
                },
            },
        }
        actual = Ingress2Istio().sync(parent, children, related)
        expected = {
            'children': [
                {
                    'apiVersion': 'networking.istio.io/v1beta1',
                    'kind': 'Gateway',
                    'metadata': {
                        'name': 'rook-ceph-mgr-dashboard-kubitus-ingress-operator',
                        'namespace': 'rook-ceph',
                    },
                    'spec': {
                        'selector': {'istio': 'ingressgateway'},
                        'servers': [
                            {
                                'port': {
                                    'number': 443,
                                    'protocol': 'HTTPS',
                                    'name': 'https-443-ingress-rook-ceph-mgr-dashboard-rook-ceph-0',
                                },
                                'hosts': ['rook-ceph.example.org'],
                                'tls': {
                                    'httpsRedirect': False,
                                    'mode': 'SIMPLE',
                                    'credentialName': 'rook-ceph.example.org-tls',
                                },
                            },
                            {
                                'port': {
                                    'number': 80,
                                    'protocol': 'HTTP',
                                    'name': 'http-80-ingress-rook-ceph-mgr-dashboard-rook-ceph',
                                },
                                'hosts': ['*'],
                                'tls': {'httpsRedirect': False},
                            },
                        ],
                    },
                },
                {
                    'apiVersion': 'networking.istio.io/v1beta1',
                    'kind': 'VirtualService',
                    'metadata': {
                        'name': 'rook-ceph-example-org-rook-ceph-mgr-dashboard-kubitus-ingress-operator',
                        'namespace': 'rook-ceph',
                    },
                    'spec': {
                        'hosts': [
                            'rook-ceph.example.org',
                        ],
                        'gateways': [
                            'rook-ceph/rook-ceph-mgr-dashboard-kubitus-ingress-operator',
                        ],
                        'http': [
                            {
                                'route': [
                                    {
                                        'destination': {
                                            'host': 'rook-ceph-mgr-dashboard.rook-ceph.svc.cluster.local',
                                            'port': {
                                                'number': 42,
                                            },
                                        },
                                        'weight': 100,
                                    },
                                ],
                                'match': [
                                    {
                                        'uri': {
                                            'prefix': '/',
                                        },
                                    },
                                ],
                            },
                        ],
                    },
                },
            ],
            'status': {
                'loadBalancer': {
                    'ingress': [{'hostname': 'rook-ceph.example.org'}],
                },
            },
        }
        self.assertEqual(actual, expected)

    def test_sync_no_matching_ingress_class(self) -> None:
        """Test sync method, with no matching ingress class."""
        parent = ingress(
            'rook-ceph-mgr-dashboard',
            'rook-ceph',
            'rook-ceph.example.org',
            'rook-ceph-mgr-dashboard',
            'http-dashboard',
            annotations={'cert-manager.io/cluster-issuer': 'ca-issuer'},
        )

        children: list = []
        related = {
            'IngressClass.networking.k8s.io/v1': {
                'kubitus-ingress-operator': ingress_class(
                    'kubitus-ingress-operator',
                ),
            },
            'Service.v1': {
                'rook-ceph-mgr-dashboard': {
                    'spec': {
                        'ports': [{'name': 'http-dashboard', 'port': 42}],
                    },
                },
            },
        }
        actual = Ingress2Istio().sync(parent, children, related)
        self.assertEqual(actual, {})

    def test_sync_ingress_class_annotations(self) -> None:
        """Test sync method; with non-default ingress class with annotations."""
        parent = ingress(
            'rook-ceph-mgr-dashboard',
            'rook-ceph',
            'rook-ceph.example.org',
            'rook-ceph-mgr-dashboard',
            'http-dashboard',
            ingress_class_name='foobar',
        )

        children: list = []
        related = {
            'IngressClass.networking.k8s.io/v1': {
                'foobar': ingress_class(
                    'foobar',
                    annotations={
                        # 'kubitus-project.gitlab.io/secret-namespace': 'istio-system',
                        'kubitus-project.gitlab.io/secret-name': 'ingress-{namespace}-{secret_name}',
                        'kubitus-project.gitlab.io/https-redirect': 'true',
                    },
                ),
            },
            'Service.v1': {
                'rook-ceph-mgr-dashboard': {
                    'spec': {
                        'ports': [{'name': 'http-dashboard', 'port': 42}],
                    },
                },
            },
        }
        actual = Ingress2Istio().sync(parent, children, related)
        expected = {
            'children': [
                {
                    'apiVersion': 'networking.istio.io/v1beta1',
                    'kind': 'Gateway',
                    'metadata': {
                        'name': 'rook-ceph-mgr-dashboard-kubitus-ingress-operator',
                        'namespace': 'rook-ceph',
                    },
                    'spec': {
                        'selector': {'istio': 'ingressgateway'},
                        'servers': [
                            {
                                'port': {
                                    'number': 443,
                                    'protocol': 'HTTPS',
                                    'name': 'https-443-ingress-rook-ceph-mgr-dashboard-rook-ceph-0',
                                },
                                'hosts': ['rook-ceph.example.org'],
                                'tls': {
                                    'httpsRedirect': False,
                                    'mode': 'SIMPLE',
                                    'credentialName': 'ingress-rook-ceph-rook-ceph.example.org-tls',
                                },
                            },
                            {
                                'port': {
                                    'number': 80,
                                    'protocol': 'HTTP',
                                    'name': 'http-80-ingress-rook-ceph-mgr-dashboard-rook-ceph',
                                },
                                'hosts': ['rook-ceph.example.org'],
                                'tls': {'httpsRedirect': True},
                            },
                        ],
                    },
                },
                {
                    'apiVersion': 'networking.istio.io/v1beta1',
                    'kind': 'VirtualService',
                    'metadata': {
                        'name': 'rook-ceph-example-org-rook-ceph-mgr-dashboard-kubitus-ingress-operator',
                        'namespace': 'rook-ceph',
                    },
                    'spec': {
                        'hosts': [
                            'rook-ceph.example.org',
                        ],
                        'gateways': [
                            'rook-ceph/rook-ceph-mgr-dashboard-kubitus-ingress-operator',
                        ],
                        'http': [
                            {
                                'route': [
                                    {
                                        'destination': {
                                            'host': 'rook-ceph-mgr-dashboard.rook-ceph.svc.cluster.local',
                                            'port': {
                                                'number': 42,
                                            },
                                        },
                                        'weight': 100,
                                    },
                                ],
                                'match': [
                                    {
                                        'uri': {
                                            'prefix': '/',
                                        },
                                    },
                                ],
                            },
                        ],
                    },
                },
            ],
            'status': {
                'loadBalancer': {
                    'ingress': [{'hostname': 'rook-ceph.example.org'}],
                },
            },
        }
        self.assertEqual(actual, expected)

    def test_sync_valid_dns1123_label(self) -> None:
        """Test sync method; too long name."""
        parent = ingress(
            'prometheus-stack-kube-prom-alertmanager',
            'prometheus-stack',
            'alertmanager.example.org',
            'prometheus-stack-kube-prom-alertmanager',
            9093,
            ingress_class_name='kubitus-ingress-operator',
        )

        children: list = []
        related = {
            'IngressClass.networking.k8s.io/v1': {
                'kubitus-ingress-operator': ingress_class(
                    'kubitus-ingress-operator',
                ),
            },
            'Service.v1': {
                'rook-ceph-mgr-dashboard': {
                    'spec': {
                        'ports': [{'name': 'http-dashboard', 'port': 42}],
                    },
                },
            },
        }
        actual = Ingress2Istio().sync(parent, children, related)
        expected = {
            'children': [
                {
                    'apiVersion': 'networking.istio.io/v1beta1',
                    'kind': 'Gateway',
                    'metadata': {
                        'name': '49884b959bb98fef4d66d70f9c2bf41add58911d399fdf5d750953974c5d099',
                        'namespace': 'prometheus-stack',
                    },
                    'spec': {
                        'selector': {'istio': 'ingressgateway'},
                        'servers': [
                            {
                                'port': {
                                    'number': 443,
                                    'protocol': 'HTTPS',
                                    'name': 'https-443-ingress-prometheus-stack-kube-prom-alertmanager-prometheus-stack-0',  # noqa: E501
                                },
                                'hosts': ['alertmanager.example.org'],
                                'tls': {
                                    'httpsRedirect': False,
                                    'mode': 'SIMPLE',
                                    'credentialName': 'alertmanager.example.org-tls',
                                },
                            },
                            {
                                'port': {
                                    'number': 80,
                                    'protocol': 'HTTP',
                                    'name': 'http-80-ingress-prometheus-stack-kube-prom-alertmanager-prometheus-stack',
                                },
                                'hosts': ['*'],
                                'tls': {'httpsRedirect': False},
                            },
                        ],
                    },
                },
                {
                    'apiVersion': 'networking.istio.io/v1beta1',
                    'kind': 'VirtualService',
                    'metadata': {
                        'name': 'alertmanager-example-org-prometheus-stack-kube-prom-alertmanager-kubitus-ingress-operator',  # noqa: E501
                        'namespace': 'prometheus-stack',
                    },
                    'spec': {
                        'hosts': [
                            'alertmanager.example.org',
                        ],
                        'gateways': [
                            'prometheus-stack/49884b959bb98fef4d66d70f9c2bf41add58911d399fdf5d750953974c5d099',
                        ],
                        'http': [{
                            'route': [{
                                'destination': {
                                    'host': 'prometheus-stack-kube-prom-alertmanager.prometheus-stack.svc.cluster.local',  # noqa: E501
                                    'port': {
                                        'number': 9093,
                                    },
                                },
                                'weight': 100,
                            }],
                            'match': [
                                {
                                    'uri': {
                                        'prefix': '/',
                                    },
                                },
                            ],
                        }],
                    },
                },
            ],
            'status': {
                'loadBalancer': {
                    'ingress': [{'hostname': 'alertmanager.example.org'}],
                },
            },
        }
        self.assertEqual(actual, expected)
