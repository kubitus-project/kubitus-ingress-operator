# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Mathieu Parent <math.parent@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from unittest import TestCase

from kubitus_ingress_operator.ingress2certificate import Ingress2Certificate
from kubitus_ingress_operator.tests.fixtures import ingress, ingress_class, resource


class TestIngress2Certificate(TestCase):
    """Test Ingress2Certificate class."""

    def test_customize(self) -> None:
        """Test customize method."""
        parent = resource(
            'kubitus-project.gitlab.io/v1alpha1',
            'Ingress2CertificateGenerator',
            'kubitus-ingress-operator',
        )
        actual = Ingress2Certificate().customize(parent)
        expected = {
            'relatedResources': [
                {'apiVersion': 'networking.k8s.io/v1', 'resource': 'ingressclasses'},
                {'apiVersion': 'networking.k8s.io/v1', 'resource': 'ingresses'},
            ],
        }
        self.assertEqual(actual, expected)

    def test_sync(self) -> None:
        """Test sync method."""
        parent = resource(
            'kubitus-project.gitlab.io/v1alpha1',
            'Ingress2CertificateGenerator',
            'kubitus-ingress-operator',
        )
        children: list = []
        related = {
            'IngressClass.networking.k8s.io/v1': {
                'foobar': ingress_class('foobar'),
            },
            'Ingress.networking.k8s.io/v1': {
                'rook-ceph-mgr-dashboard': ingress(
                    'rook-ceph-mgr-dashboard',
                    'rook-ceph',
                    'rook-ceph.example.org',
                    'rook-ceph-mgr-dashboard',
                    'http-dashboard',
                    annotations={'cert-manager.io/cluster-issuer': 'ca-issuer'},
                ),
            },
        }
        actual = Ingress2Certificate().sync(parent, children, related)
        expected = {
            'children': [{
                'apiVersion': 'cert-manager.io/v1',
                'kind': 'Certificate',
                'metadata': {
                    'name': 'rook-ceph.example.org-tls',
                    'namespace': 'rook-ceph',
                    'labels': {},
                },
                'spec': {
                    'dnsNames': ['rook-ceph.example.org'],
                    'secretName': 'rook-ceph.example.org-tls',
                    'issuerRef': {'name': 'ca-issuer', 'kind': 'ClusterIssuer', 'group': 'cert-manager.io'},
                    'usages': ['digital signature', 'key encipherment'],
                },
            }],
        }
        self.assertEqual(actual, expected)

    def test_sync_to_istio_system(self) -> None:
        """Test sync method, with annotations."""
        parent = resource(
            'kubitus-project.gitlab.io/v1alpha1',
            'Ingress2CertificateGenerator',
            'kubitus-ingress-operator',
        )
        children: list = []
        related = {
            'IngressClass.networking.k8s.io/v1': {
                'foobar': ingress_class('foobar', annotations={
                    'kubitus-project.gitlab.io/secret-namespace': 'istio-system',
                    'kubitus-project.gitlab.io/secret-name': 'ingress-{namespace}-{secret_name}',
                }),
            },
            'Ingress.networking.k8s.io/v1': {
                'rook-ceph-mgr-dashboard': ingress(
                    'rook-ceph-mgr-dashboard',
                    'rook-ceph',
                    'rook-ceph.example.org',
                    'rook-ceph-mgr-dashboard',
                    'http-dashboard',
                    annotations={'cert-manager.io/cluster-issuer': 'ca-issuer'},
                ),
            },
        }
        actual = Ingress2Certificate().sync(parent, children, related)
        expected = {
            'children': [{
                'apiVersion': 'cert-manager.io/v1',
                'kind': 'Certificate',
                'metadata': {
                    'name': 'ingress-rook-ceph-rook-ceph.example.org-tls',
                    'namespace': 'istio-system',
                    'labels': {},
                },
                'spec': {
                    'dnsNames': ['rook-ceph.example.org'],
                    'secretName': 'ingress-rook-ceph-rook-ceph.example.org-tls',
                    'issuerRef': {'name': 'ca-issuer', 'kind': 'ClusterIssuer', 'group': 'cert-manager.io'},
                    'usages': ['digital signature', 'key encipherment'],
                },
            }],
        }
        self.assertEqual(actual, expected)
