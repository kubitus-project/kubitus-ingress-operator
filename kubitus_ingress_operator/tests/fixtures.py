# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Mathieu Parent <math.parent@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from typing import Optional, Union

from kubitus_ingress_operator.constants import CONTROLLER_NAME


def resource(
    api_version: str,
    kind: str,
    name: str,
    namespace: Optional[str] = None,
    spec: Optional[dict] = None,
    labels: Optional[dict[str, str]] = None,
    annotations: Optional[dict[str, str]] = None,
) -> dict:
    """Get a Kubernetes resource.

    Args:
        api_version: apiVersion.
        kind: resource kind.
        name: resource name.
        namespace: resource namespace.
        spec: resource name.
        labels: resource labels.
        annotations: resource annotations.

    Returns:
        A Kubernetes resource.
    """
    res = {
        'apiVersion': api_version,
        'kind': kind,
        'metadata': {
            'name': name,
            'namespace': namespace,
            'labels': labels or {},
            'annotations': annotations or {},
        },
    }
    if spec:
        res['spec'] = spec
    return res


def ingress_class(
    name: str,
    controller: str = CONTROLLER_NAME,
    labels: Optional[dict[str, str]] = None,
    annotations: Optional[dict[str, str]] = None,
) -> dict:
    """Get a Kubernetes IngressClass.

    Args:
        name: resource name.
        controller: Ingress class controller.
        labels: resource labels.
        annotations: resource annotations.

    Returns:
        A Kubernetes IngressClass.
    """
    spec = {
        'controller': controller,
    }
    return resource(
        'networking.k8s.io/v1',
        'IngressClass',
        name,
        None,  # namespace
        spec,
        labels,
        annotations,
    )


def ingress(
    name: str,
    namespace: str,
    host: str,
    service_name: str,
    service_port: Union[int, str],
    labels: Optional[dict[str, str]] = None,
    annotations: Optional[dict[str, str]] = None,
    ingress_class_name: Optional[str] = None,
) -> dict:
    """Get a Kubernetes Ingress.

    Args:
        name: resource name.
        namespace: resource namespace.
        host: Host FQDN.
        service_name: Service name.
        service_port: Either port name or port number.
        labels: resource labels.
        annotations: resource annotations.
        ingress_class_name: Ingress class name.

    Returns:
        A Kubernetes Ingress.
    """
    if isinstance(service_port, int):
        service_port_spec: dict[str, Union[int, str]] = {'number': service_port}
    else:
        service_port_spec = {'name': service_port}
    spec = {
        'rules': [
            {
                'host': host,
                'http': {
                    'paths': [
                        {
                            'backend': {
                                'service': {
                                    'name': service_name,
                                    'port': service_port_spec,
                                },
                            },
                            'path': '/',
                            'pathType': 'Prefix',
                        },
                    ],
                },
            },
        ],
        'tls': [
            {
                'hosts': [
                    host,
                ],
                'secretName': '{0}-tls'.format(host),
            },
        ],
    }
    if ingress_class_name:
        spec['ingressClassName'] = ingress_class_name
    return resource(
        'networking.k8s.io/v1',
        'Ingress',
        name,
        namespace,
        spec,
        labels,
        annotations,
    )
