# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Mathieu Parent <math.parent@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import annotations

import logging
import re
from hashlib import sha256
from typing import Any, Optional, Tuple

from kubitus_ingress_operator.constants import CONTROLLER_NAME


logger = logging.getLogger(__name__)


class ServiceLister(object):
    def __init__(self, services: dict):
        self._services = services
        self._namespace: Optional[str] = None

    def services(self, namespace: str) -> ServiceLister:
        self._namespace = namespace
        return self

    def get(self, service_name: str) -> Tuple[dict, Optional[str]]:
        svc = self._services.get(service_name, None)
        return svc, None


class Ingress2Istio(object):  # noqa: WPS338
    def customize(self, ingress: dict) -> dict:  # noqa: WPS210
        # FIXME This is no-op, because:
        # "If the parent resource is namespaced, the related resources must come from the same namespace"
        # https://metacontroller.github.io/metacontroller/api/customize.html#customize-hook-response
        related_resources: list[dict[str, Any]] = [
            {
                'apiVersion': 'networking.k8s.io/v1',
                'resource': 'ingressclasses',
            },
        ]

        ingress_namespace = ingress.get('metadata', {}).get('namespace', None)
        ingress_spec_rules = ingress.get('spec', {}).get('rules', {})

        names = []

        for rule in ingress_spec_rules:
            host = rule.get('host')
            if rule.get('http') is None:
                logger.info(
                    'invalid ingress rule %s:%s for host %s, no paths defined',
                    ingress_namespace,
                    ingress.get('metadata', {}).get('name', None),
                    host,
                )
                continue

            for http_path in rule.get('http').get('paths'):
                name = http_path.get('backend', {}).get('service', {}).get('name')
                if name is not None:
                    names.append(name)

        if names:
            related_resources.append({
                'apiVersion': 'v1',
                'resource': 'services',
                'namespace': ingress_namespace,
                'names': names,
            })

        return {
            'relatedResources': related_resources,
        }

    def sync(self, ingress: dict, children: list, related: dict) -> dict:  # noqa: WPS210
        domain_suffix = 'cluster.local'
        ingress_classes = related.get('IngressClass.networking.k8s.io/v1', {}).values()
        # FIXME Default IngressClass
        if not ingress_classes:
            ingress_classes = [{
                'apiVersion': 'networking.k8s.io/v1',
                'kind': 'IngressClass',
                'metadata': {
                    'name': 'kubitus-ingress-operator',
                    'labels': {},
                    'annotations': {
                        'ingressclass.kubernetes.io/is-default-class': 'true',
                        'kubitus-project.gitlab.io/secret-namespace': 'istio-system',
                        'kubitus-project.gitlab.io/secret-name': 'ingress-{namespace}-{secret_name}',
                    },
                },
                'spec': {
                    'controller': 'kubitus-project.gitlab.io/kubitus-ingress-operator',
                },
            }]

        ingress_class = self._get_ingress_class(
            ingress_classes,
            ingress.get('spec', {}).get('ingressClassName'),
        )
        if not ingress_class:
            return {}
        mesh: dict = {}

        ingress_by_host: dict = {}
        service_lister = ServiceLister(related.get('Service.v1'))  # type: ignore

        gateway_config = self.convert_ingress_v1beta1(ingress, mesh, ingress_class)
        self.convert_ingress_virtual_service(ingress, domain_suffix, ingress_by_host, service_lister)

        # FIXME ips of istio-ingressgateway should be returned instead
        ingress_status_lb_ingress = []
        for rule in ingress.get('spec', {}).get('rules', []):
            ingress_status_lb_ingress.append({
                'hostname': rule.get('host').replace('*', 'any'),
            })

        return {
            'children': [gateway_config] + list(ingress_by_host.values()),
            'status': {
                'loadBalancer': {'ingress': ingress_status_lb_ingress},
            },
        }

    def _get_ingress_class(self, ingress_classes: list[dict], ingress_class_name: str) -> Optional[dict]:
        current_ingress_class = None
        for ingress_class in ingress_classes:
            if ingress_class.get('spec', {}).get('controller') != CONTROLLER_NAME:
                continue
            if ingress_class_name == ingress_class.get('metadata', {}).get('name'):
                current_ingress_class = ingress_class
            ingress_class_annotations = ingress_class.get('metadata', {}).get('annotations', {})
            ingress_class_is_default = ingress_class_annotations.get('ingressclass.kubernetes.io/is-default-class')
            if not current_ingress_class and ingress_class_is_default == 'true':
                current_ingress_class = ingress_class
        return current_ingress_class

    #  ConvertIngressV1alpha3
    # https://github.com/istio/istio/blob/4ecbb5e8d193ce59687a79f27cf84c98124f8e52/pilot/pkg/config/kube/ingressv1/conversion.go#L75
    # https://istio.io/latest/docs/reference/config/networking/gateway/
    def convert_ingress_v1beta1(self, ingress: dict, mesh: dict, ingress_class: dict) -> dict:  # noqa: WPS210
        istio_ingress_gateway_name = 'kubitus-ingress-operator'  # 'istio-autogenerated-k8s-ingress'
        ingress_name = ingress.get('metadata', {}).get('name', None)
        ingress_namespace = ingress.get('metadata', {}).get('namespace', None)
        ingress_spec = ingress.get('spec', {})
        ingress_spec_tls = ingress_spec.get('tls', {})

        ingressclass_annotations = ingress_class.get('metadata', {}).get('annotations') or {}
        ingressclass_secret_name = ingressclass_annotations.get(
            'kubitus-project.gitlab.io/secret-name', '{secret_name}',
        )
        ingressclass_https_redirect = ingressclass_annotations.get(
            'kubitus-project.gitlab.io/https-redirect', 'false',
        )

        # Gateway
        gateway: dict[str, Any] = {}
        gateway['selector'] = self.get_ingress_gateway_selector(
            mesh.get('ingressSelector'),  # type: ignore
            mesh.get('ingressService'),  # type: ignore
        )
        gateway['servers'] = []

        all_tls_hosts = []
        for idx, tls in enumerate(ingress_spec_tls):
            tls_secret_name = tls.get('secretName')
            tls_hosts = tls.get('hosts', [])
            if not tls_secret_name:
                logger.info(
                    'invalid ingress rule %s:%s for hosts %s, no secretName defined',
                    ingress_namespace,
                    ingress_name,
                    tls_hosts,
                )
                continue
            gateway_secret_name = ingressclass_secret_name.format(
                namespace=ingress_namespace,
                secret_name=tls_secret_name,
            )
            if not tls_hosts:
                tls_hosts = ['*']
            all_tls_hosts.extend(tls_hosts)
            gateway['servers'].append({
                'port': {
                    'number': 443,
                    'protocol': 'HTTPS',
                    'name': 'https-443-ingress-{0}-{1}-{2}'.format(ingress_name, ingress_namespace, idx),
                },
                'hosts': tls_hosts,
                'tls': {
                    'httpsRedirect': False,
                    'mode': 'SIMPLE',
                    'credentialName': gateway_secret_name,
                },
            })

        if all_tls_hosts and ingressclass_https_redirect == 'true':
            gateway['servers'].append({
                'port': {
                    'number': 80,
                    'protocol': 'HTTP',
                    'name': 'http-80-ingress-{0}-{1}'.format(ingress_name, ingress_namespace),
                },
                'hosts': all_tls_hosts,
                'tls': {
                    'httpsRedirect': True,
                },
            })
        else:
            gateway['servers'].append({
                'port': {
                    'number': 80,
                    'protocol': 'HTTP',
                    'name': 'http-80-ingress-{0}-{1}'.format(ingress_name, ingress_namespace),
                },
                'hosts': [
                    '*',
                ],
                'tls': {
                    'httpsRedirect': False,
                },
            })

        return {
            'apiVersion': 'networking.istio.io/v1beta1',
            'kind': 'Gateway',
            'metadata': {
                'name': self._ensure_dns1123_label('{0}-{1}'.format(ingress_name, istio_ingress_gateway_name)),
                'namespace': ingress_namespace,
            },
            'spec': gateway,
        }

    def convert_ingress_virtual_service(  # noqa: WPS210,WPS231
        self,
        ingress: dict,
        domain_suffix: str,
        ingress_by_host: dict,
        service_lister: ServiceLister,
    ) -> None:
        istio_ingress_gateway_name = 'kubitus-ingress-operator'  # 'istio-autogenerated-k8s-ingress'
        ingress_name = ingress.get('metadata', {}).get('name', None)
        ingress_namespace = ingress.get('metadata', {}).get('namespace', None)
        ingress_spec = ingress.get('spec', {})
        ingress_spec_rules = ingress_spec.get('rules', {})

        http_routes = []
        for rule in ingress_spec_rules:
            host = rule.get('host')
            if rule.get('http') is None:
                logger.info(
                    'invalid ingress rule %s:%s for host %s, no paths defined',
                    ingress_namespace,
                    ingress_name,
                    host,
                )
                continue
            name_prefix = host.replace('.', '-')
            if host == '':
                host = '*'
            virtual_service = {
                'hosts': [
                    host,
                ],
                'gateways': [
                    '{0}/{1}'.format(
                        ingress_namespace,
                        self._ensure_dns1123_label('{0}-{1}'.format(ingress_name, istio_ingress_gateway_name)),
                    ),
                ],
            }

            for http_path in rule.get('http').get('paths'):
                http_match = {}
                path_type = http_path.get('pathType')
                if path_type is not None:
                    if path_type == 'Exact':
                        http_match['uri'] = {
                            'exact': http_path.get('path'),
                        }
                    elif path_type == 'Prefix':
                        path = http_path.get('path')
                        if not path.endswith('/'):
                            path += '/'  # noqa: WPS336
                        http_match['uri'] = {
                            'prefix': path,
                        }
                    else:
                        http_match['uri'] = self._create_fallback_string_match(http_path.get('path', ''))
                else:
                    http_match['uri'] = self._create_fallback_string_match(http_path.get('path', ''))

                http_route = self._ingress_backend_to_http_route(
                    http_path.get('backend'),
                    ingress_namespace,
                    domain_suffix,
                    service_lister,
                )
                if http_route is None:
                    logger.info(
                        'invalid ingress rule %s:%s for host %s, no backend defined for path',
                        ingress_namespace,
                        ingress_name,
                        host,
                    )
                    continue
                http_route['match'] = [http_match]
                http_routes.append(http_route)

            virtual_service['http'] = http_routes

            virtual_service_config = {
                'apiVersion': 'networking.istio.io/v1beta1',
                'kind': 'VirtualService',
                'metadata': {
                    'name': '{0}-{1}-{2}'.format(name_prefix, ingress_name, istio_ingress_gateway_name),
                    'namespace': ingress_namespace,
                },
                'spec': virtual_service,
            }

            if host in ingress_by_host:
                logger.warning('Merging ingress by host is not supported. The last one wins')
            ingress_by_host[host] = virtual_service_config

    @classmethod
    def _is_dns1123_label(cls, label: str) -> bool:
        return len(label) <= 63 and bool(re.match('^[a-zA-Z0-9](?:[-a-zA-Z0-9]*[a-zA-Z0-9])?$', label))

    @classmethod
    def _ensure_dns1123_label(cls, label: str) -> str:
        if cls._is_dns1123_label(label):
            return label
        return sha256(label.encode('utf-8')).hexdigest()[:63]

    def _ingress_backend_to_http_route(
        self,
        backend: Optional[dict],
        namespace: str,
        domain_suffix: str,
        service_lister: ServiceLister,
    ) -> Optional[dict]:
        if backend is None:
            return None

        port = {}

        if backend.get('service') is None:
            logger.info('backend service must be specified')
            return None
        service_port = backend.get('service', {}).get('port', {})
        if service_port.get('number', 0) > 0:
            port['number'] = service_port.get('number', 0)
        else:
            resolved_port, err = self.resolve_named_port(backend, namespace, service_lister)
            if err is not None:
                logger.info(
                    'failed to resolve named port %s, error: %s',
                    backend.get('service', {}).get('port', {}).get('name'),
                    err,
                )
                return None
            port['number'] = resolved_port

        return {
            'route': [
                {
                    'destination': {
                        'host': '{0}.{1}.svc.{2}'.format(
                            backend.get('service', {}).get('name'),
                            namespace,
                            domain_suffix,
                        ),
                        'port': port,
                    },
                    'weight': 100,
                },
            ],
        }

    def resolve_named_port(
        self,
        backend: dict,
        namespace: str,
        service_lister: ServiceLister,
    ) -> Tuple[int, Optional[str]]:
        svc, err = service_lister.services(namespace).get(
            backend.get('service', {}).get('name'),
        )
        if err is not None:
            return 0, err
        service_port_name = backend.get('service', {}).get('port', {}).get('name')
        for port in svc.get('spec', {}).get('ports', []):
            if port.get('name') == service_port_name:
                return port.get('port', 0), None
        return 0, 'item not found'

    # createFallbackStringMatch
    def _create_fallback_string_match(self, path: str) -> dict[str, str]:
        if path == '':
            return {}

        # Note that this implementation only converts prefix and exact matches, not regexps.

        # Replace e.g. 'foo.*' with prefix match
        if path.endswith('.*'):
            return {
                'prefix': path.removesuffix('.*'),
            }
        if path.endswith('/*'):
            return {
                'prefix': path.removesuffix('/*'),
            }

        # Replace e.g. 'foo' with a exact match
        return {
            'exact': path,
        }

    def get_ingress_gateway_selector(
        self,
        ingress_selector: dict,
        ingress_service: str,
    ) -> dict[str, Any]:
        if ingress_selector:
            return {'istio': ingress_selector}
        elif ingress_service != 'istio-ingressgateway' and ingress_service:
            return {'istio': ingress_service}
        return {'istio': 'ingressgateway'}
