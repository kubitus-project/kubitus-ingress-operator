# -*- coding: utf-8 -*-
#
# Copyright (C) 2021 Mathieu Parent <math.parent@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import annotations

import logging
from typing import Any, Optional

from kubitus_ingress_operator.constants import CONTROLLER_NAME


OptionalStr = Optional[str]

logger = logging.getLogger(__name__)


class Ingress2Certificate(object):  # noqa: WPS338
    def customize(self, parent: dict) -> dict[str, list]:
        return {
            'relatedResources': [
                {
                    'apiVersion': 'networking.k8s.io/v1',
                    'resource': 'ingressclasses',
                },
                {
                    'apiVersion': 'networking.k8s.io/v1',
                    'resource': 'ingresses',
                },
            ],
        }

    # https://github.com/jetstack/cert-manager/blob/06b68d35e0f5e0acbf8387cf7332e064f5c1c873/pkg/controller/ingress-shim/sync.go#L324
    def sync(self, parent: dict, children: list[dict], related: dict) -> dict:  # noqa: WPS210, WPS231
        certificates = []
        for _, ingclass in related['IngressClass.networking.k8s.io/v1'].items():
            if ingclass.get('spec', {}).get('controller') != CONTROLLER_NAME:
                continue
            for _, ing in related['Ingress.networking.k8s.io/v1'].items():
                issuer_name, issuer_kind, issuer_group, _ = self._issuer_for_ingress(ing)
                if issuer_name and issuer_kind and issuer_group:
                    certificates.extend(
                        self._build_certificates(ingclass, ing, issuer_name, issuer_kind, issuer_group),
                    )
        return {'children': certificates}

    def _validate_ingress(self, ing: dict) -> list[str]:  # noqa: WPS210
        errs: list[str] = []
        named_secrets: dict[str, int] = {}
        for tls in ing.get('spec', {}).get('tls', []):
            secret_name = tls.get('secretName', '')
            named_secrets[secret_name] = named_secrets.get(secret_name, 0) + 1
        for name, name_count in named_secrets.items():
            if name_count > 1:
                errs.append('Duplicate TLS entry for secretName {0}'.format(name))
        return errs

    def _validate_ingress_tls_block(self, tls_block: dict) -> list[str]:
        errs: list[str] = []

        if not tls_block.get('hosts', []):
            errs.append('secret {0} for ingress TLS has no hosts specified'.format(tls_block.get('secretName', '')))
        if not tls_block.get('secretName', ''):
            errs.append('TLS entry for hosts {0} must specify a secretName'.format(tls_block.get('hosts', [])))
        return errs

    def _build_certificates(  # noqa: WPS210
        self,
        ctx: dict,
        ing: dict,
        issuer_name: str,
        issuer_kind: str,
        issuer_group: str,
    ) -> list[dict]:
        certificates: list[dict] = []
        ing_spec_tls = ing.get('spec', {}).get('tls', {})
        for idx, tls in enumerate(ing_spec_tls):
            errs = self._validate_ingress_tls_block(tls)
            if errs:
                err_msg = ', '.join(errs)
                logger.warning('TLS entry %d is invalid: %s', idx, err_msg)
                continue
            ingressclass_annotations = ctx.get('metadata', {}).get('annotations') or {}
            certificate_namespace = ingressclass_annotations.get(
                'kubitus-project.gitlab.io/secret-namespace', '{namespace}',
            ).format(
                namespace=ing.get('metadata', {}).get('namespace', ''),
                secret_name=tls.get('secretName', ''),
            )
            certificate_name = ingressclass_annotations.get(
                'kubitus-project.gitlab.io/secret-name', '{secret_name}',
            ).format(
                namespace=ing.get('metadata', {}).get('namespace', ''),
                secret_name=tls.get('secretName', ''),
            )
            crt = {
                'apiVersion': 'cert-manager.io/v1',
                'kind': 'Certificate',
                'metadata': {
                    'name': certificate_name,
                    'namespace': certificate_namespace,
                    'labels': ing.get('metadata', {}).get('labels', {}),
                },
                'spec': {
                    'dnsNames': tls.get('hosts', []),
                    'secretName': certificate_name,
                    'issuerRef': {
                        'name': issuer_name,
                        'kind': issuer_kind,
                        'group': issuer_group,
                    },
                    'usages': [
                        'digital signature',
                        'key encipherment',
                    ],
                },
            }
            certificates.append(crt)

        return certificates

    def _issuer_for_ingress(  # noqa: WPS210
        self,
        ing: dict,
    ) -> tuple[OptionalStr, OptionalStr, OptionalStr, OptionalStr]:
        errs: list[str] = []

        name = self.defaults.get('issuerName')
        kind = self.defaults.get('issuerKind')
        group = self.defaults.get('issuerGroup')
        metadata_annotations = ing.get('metadata', {}).get('annotations', {})

        issuer_name = metadata_annotations.get('cert-manager.io/issuer')
        if issuer_name:
            name = issuer_name
            kind = 'Issuer'

        cluster_issuer_name = metadata_annotations.get('cert-manager.io/cluster-issuer')
        if cluster_issuer_name:
            name = cluster_issuer_name
            kind = 'ClusterIssuer'

        kind_name = metadata_annotations.get('cert-manager.io/issuer-kind')
        if kind_name:
            kind = kind_name

        group_name = metadata_annotations.get('cert-manager.io/issuer-group')
        if group_name:
            group = group_name

        if not name:
            errs.append('failed to determine issuer name to be used for ingress resource')

        if issuer_name and cluster_issuer_name:
            errs.append('both cert-manager.io/issuer and cert-manager.io/cluster-issuer may not be set'.format())

        if cluster_issuer_name and group_name:
            errs.append('both cert-manager.io/cluster-issuer and cert-manager.io/issuer-group may not be set'.format())

        if cluster_issuer_name and kind_name:
            errs.append('both cert-manager.io/cluster-issuer and cert-manager.io/issuer-kind may not be set'.format())

        if errs:
            return '', '', '', ', '.join(errs)

        return name, kind, group, None

    @property
    def defaults(self) -> dict[str, Any]:
        return {
            'autoCertificateAnnotations': ['kubernetes.io/tls-acme'],
            'issuerName': '',
            'issuerKind': 'Issuer',
            'issuerGroup': 'cert-manager.io',
        }
