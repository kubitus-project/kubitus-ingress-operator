# -*- coding: utf-8 -*-
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import logging
import sys
from argparse import ArgumentParser
from http.server import HTTPServer

import kubitus_ingress_operator
from kubitus_ingress_operator.controller import Controller


DEFAULT_ADDRESS = ''
DEFAULT_PORT = 8080

logger = logging.getLogger(__name__)


def _get_parser() -> ArgumentParser:
    parser = ArgumentParser(description='Kubitus Ingress Operator')
    parser.add_argument('--version', help='Display the version.', action='store_true')
    parser.add_argument('-v', '--verbose', '--fancy', help='Verbose mode', action='store_true')
    parser.add_argument('-d', '--debug', help='Debug mode (display HTTP requests)', action='store_true')
    parser.add_argument('--address', help='HTTP address', default=DEFAULT_ADDRESS)
    parser.add_argument('-p', '--port', help='HTTP port', type=int, default=DEFAULT_PORT)
    return parser


def main() -> None:
    """Run application (main entry point)."""
    parser = _get_parser()

    args = parser.parse_args(sys.argv[1:])

    if args.version:
        print(kubitus_ingress_operator.__version__)  # noqa: WPS421
        sys.exit(0)

    verbose = args.verbose
    debug = args.debug

    log_level = logging.WARNING
    if verbose:
        log_level = logging.INFO
    if debug:
        log_level = logging.DEBUG
    logging.basicConfig(format='%(levelname)-8.8s %(message)s', level=log_level)

    logger.info('Starting server %s:%s', args.address, args.port)
    HTTPServer((args.address, args.port), Controller).serve_forever()


if __name__ == '__main__':
    main()
