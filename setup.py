#!/usr/bin/python3
# -*- coding: utf-8 -*-

from setuptools import find_packages, setup

import kubitus_ingress_operator


with open('README.md', 'r') as readme_file:
    readme = readme_file.read()

setup(
    name='kubitus-ingress-operator',
    version=kubitus_ingress_operator.__version__,
    description='Kubitus Ingress Operator',
    long_description=readme,
    long_description_content_type='text/markdown',
    author=kubitus_ingress_operator.__author__,  # noqa: WPS609
    author_email=kubitus_ingress_operator.__email__,  # noqa: WPS609
    license=kubitus_ingress_operator.__license__,  # noqa: WPS609
    url='https://gitlab.com/kubitus-project/kubitus-ingress-operator',
    packages=find_packages(),
    package_data={
    },
    install_requires=[
    ],
    entry_points={
        'console_scripts': [
            'kubitus-ingress-operator = kubitus_ingress_operator.cli:main',
        ],
    },
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Web Environment',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: Apache Software License',
        'Natural Language :: English',
        'Operating System :: POSIX',
        'Operating System :: Microsoft :: Windows',
        'Programming Language :: Python :: 3 :: Only',
    ],
    keywords='Kubernetes ingress controller operator',
)
