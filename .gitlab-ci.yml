stages:
  - build
  - test
  - deploy  # dummy stage to follow the template guidelines
  - review
  - dast
  - staging
  - canary
  - production
  - incremental rollout 10%
  - incremental rollout 25%
  - incremental rollout 50%
  - incremental rollout 100%
  - performance
  - cleanup

include:
  - template: 'Workflows/Branch-Pipelines.gitlab-ci.yml'
  - template: Security/DAST.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/DAST.gitlab-ci.yml
  - template: Security/Container-Scanning.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/Container-Scanning.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/Dependency-Scanning.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/License-Scanning.gitlab-ci.yml
  - template: Security/SAST.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/SAST.gitlab-ci.yml
  - template: Security/Secret-Detection.gitlab-ci.yml  # https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Security/Secret-Detection.gitlab-ci.yml

commitlint:
  image: node:21.5.0
  stage: test
  before_script:
  - npm install -g commitlint @commitlint/{config-conventional,cli}
  script:
  - npx commitlint --from=${CI_MERGE_REQUEST_DIFF_BASE_SHA:-HEAD^}
  interruptible: true
  needs: []

container_scanning:
  allow_failure: false
  variables:
    GIT_STRATEGY: fetch
  before_script:
    - . ci/vars.sh
    # https://docs.gitlab.com/ee/user/application_security/container_scanning/#available-cicd-variables
    - export CS_IMAGE="$image_repository:$kio_version"

variables:
  HELM_VERSION: v3.13.3


build-image:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:v1.19.2-debug
    entrypoint: [""]
  before_script:
    - mkdir -p /kaniko/.docker
    - auth="$(echo -n "$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD" | base64)"
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"auth\":\"$auth\"}}}" > /kaniko/.docker/config.json
  script:
    - . ci/vars.sh
    - set -x
    - /kaniko/executor --context "$CI_PROJECT_DIR" $kaniko_destinations


build-helm:
  stage: build
  image: debian:bullseye-20240110
  needs: []
  before_script:
    - apt-get update
    - apt-get install -y curl git tar
    - curl -fLSs https://get.helm.sh/helm-$HELM_VERSION-linux-amd64.tar.gz -o helm.tar.gz
    - tar -xvf helm.tar.gz
    - mv linux-amd64/helm /usr/local/bin/helm
    - helm plugin install https://github.com/chartmuseum/helm-push.git
    - git clone https://github.com/metacontroller/metacontroller.git
    - metacontroller_version="$(cat deploy/METACONTROLLER_VERSION)"
    - git -C metacontroller reset --hard "$metacontroller_version"
  script:
    - . ci/vars.sh
    - echo "helm_channel=$helm_channel kio_version=$kio_version"
    - mkdir deploy/helm/kubitus-ingress-operator/charts
    - cp -a metacontroller/deploy/helm/metacontroller -d deploy/helm/kubitus-ingress-operator/charts/metacontroller
    - sed -i s/metacontroller-helm/metacontroller/ deploy/helm/kubitus-ingress-operator/charts/metacontroller/Chart.yaml
    - helm package --version "$kio_version" --app-version "$kio_version" deploy/helm/kubitus-ingress-operator
    - helm repo add --username gitlab-ci-token --password "$CI_JOB_TOKEN" kubitus-ingress-operator "$CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/helm/$helm_channel"
    - helm cm-push "kubitus-ingress-operator-$kio_version.tgz" kubitus-ingress-operator
    - mkdir build
    - helm template --include-crds kubitus-ingress-operator --set image.tag=latest --set metacontroller.enabled=true --set resources.limits.memory=128Mi --set resources.limits.cpu=250m --set metacontroller.namespaceOverride=metacontroller "kubitus-ingress-operator-$kio_version.tgz" > build/manifests.yaml
    - sed -i -e /helm/d -e /app.kubernetes.io\.version/d build/manifests.yaml
    - diff -ur build/manifests.yaml deploy/manifests.yaml


test bullseye:
  stage: test
  image: debian:bullseye-20240110
  needs: []
  before_script:
    - apt-get update
    - apt-get install -y python3 tox
  script:
    - tox
