# Kubitus Ingress Operator

:warning: **Unmaintained projet** :warning:

:recycle: Project will be archived in 2024.

## Summary

Istio ingress gateways can only access secrets created in their namespace
(cf. [istio/istio#14598](https://github.com/istio/istio/issues/14598))
and `cert-manager` can only create secrets in its namespace
(cf. [jetstack/cert-manager#2522](https://github.com/jetstack/cert-manager/issues/2522)).
This operator is a workaround for those two bugs.

It is implemented as a Kubernetes operator, to create, for matching ingresses:

- an Istio [Gateway](https://istio.io/latest/docs/reference/config/networking/gateway/#Gateway)
- an Istio [VirtualService](https://istio.io/latest/docs/reference/config/networking/virtual-service/#VirtualService)
- a `cert-manager` [Certificate](https://cert-manager.io/docs/usage/certificate/)

Under the hood, it uses [Metacontroller](https://metacontroller.github.io/metacontroller/).

## Installation

### Pre-requisities

Ensure, you have disabled the `cert-manager`'s `ingress-shim`.
If you installed it via Helm, this can be done with the following values:

```yaml
extraArgs:
- --controllers=*,-ingress-shim
```

(`kubitus-installer` already [does this](https://gitlab.com/kubitus-project/kubitus-installer/-/blob/fac8a9c50577a514b4342f457aadbec4d5f0782a/roles/cert_manager/templates/values.yaml.j2#L6-7)).

Ensure you have disabled the `istio` ingress controller too.
This can be done in the mesh config:

```yaml
meshConfig:
    ingressControllerMode: 'OFF'
```

(`kubitus-installer` already [does this](https://gitlab.com/kubitus-project/kubitus-installer/-/blob/fac8a9c50577a514b4342f457aadbec4d5f0782a/roles/istio/templates/istioctl_values.yaml.j2#L33)).

### Using Helm

```shell
helm repo add kubitus-ingress-operator https://gitlab.com/api/v4/projects/28163461/packages/helm/stable
helm install \
  kubitus-ingress-operator kubitus-ingress-operator/kubitus-ingress-operator \
  --include-crds \
  --set metacontroller.enabled=true
```

More information [here](doc/helm-install.md).

### Using plain manifests

This installation includes [Metacontroller](https://metacontroller.github.io/metacontroller/).

Installation:

```bash
kubectl create namespace kubitus-ingress-operator metacontroller
kubectl apply -n kubitus-ingress-operator -f deploy/manifests.yaml
```

## Limitations

Kubitus Ingress Operator takes it's configuration from the [`IngressClass` annotations](deploy/helm/kubitus-ingress-operator/templates/ingressclass.yaml#L8-13),
however, this object is not reachable from `ingress2istio`.
[Hard-coded values](https://gitlab.com/kubitus-project/kubitus-ingress-operator/-/blob/main/kubitus_ingress_operator/ingress2istio.py#L98-101) are used instead.
More information [in issue #1](https://gitlab.com/kubitus-project/kubitus-ingress-operator/-/issues/1).

Kubitus Ingress Operator fills `status.loadBalancer.ingress` with `hostname`,
and not `ip`.
More information [in issue #2](https://gitlab.com/kubitus-project/kubitus-ingress-operator/-/issues/2).
