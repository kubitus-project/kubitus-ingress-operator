# Install Kubitus Ingress Operator using Helm

## Installing the chart

```shell
helm repo add kubitus-ingress-operator https://gitlab.com/api/v4/projects/28163461/packages/helm/stable
helm install --include-crds kubitus-ingress-operator kubitus-ingress-operator/kubitus-ingress-operator --set metacontroller.enabled=true
```

Note: You can replace the `stable` channel above by any branch name
for developpement versions.

## Configuration

| Parameter                                       | Description                                   | Default                                                             |
|-------------------------------------------------|-----------------------------------------------|---------------------------------------------------------------------|
| `replicaCount`                                  | Number of replicas                            | `1`                                                                 |
| `image.repository`                              | Image repository                              | `registry.gitlab.com/kubitus-project/kubitus-ingress-operator/main` |
| `image.pullPolicy`                              | Image pull policy                             | `IfNotPresent`                                                      |
| `image.tag`                                     | Image tag                                     | `""` (`Chart.AppVersion`)                                           |
| `imagePullSecrets`                              | Image pull secrets                            | `[]`                                                                |
| `nameOverride`                                  | Override the deployment name                  | `""` (`Chart.Name`)                                                 |
| `fullnameOverride`                              | Override the deployment full name             | `""` (`Release.Namespace-Chart.Name`)                               |
| `serviceAccount.create`                         | Create service account                        | `true`                                                              |
| `serviceAccount.annotations`                    | ServiceAccount annotations                    | `{}`                                                                |
| `serviceAccount.name`                           | Service account name to use, when empty will be set to created account if `serviceAccount.create` is set else to `default` | `""` |
| `podAnnotations`                                | Pod annotations                               | `{}`                                                                |
| `podSecurityContext`                            | Pod security context                          | `{}`                                                                |
| `securityContext`                               | Container security context                    | `{}`                                                                |
| `extraArgs`                                     | Additional arguments                          | `[]`                                                                |
| `service.type`                                  | Service type                                  | `ClusterIP`                                                         |
| `service.port`                                  | Service port                                  | `80`                                                                |
| `resources`                                     | CPU/Memory resource requests/limits           | `{}`                                                                |
| `autoscaling.enabled`                           | Autoscaling enabled                           | `false`                                                             |
| `autoscaling.minReplicas`                       | Autoscaling minimum number of replicas        | `1`                                                                 |
| `autoscaling.maxReplicas`                       | Autoscaling maximum number of replicas        | `100`                                                               |
| `autoscaling.targetCPUUtilizationPercentage`    | Autoscaling target CPU utilization            | `80`                                                                |
| `autoscaling.targetMemoryUtilizationPercentage` | Autoscaling target memory utilization         | `80`                                                                |
| `nodeSelector`                                  | Node labels for pod assignment                | `{}`                                                                |
| `tolerations`                                   | Toleration labels for pod assignment          | `[]`                                                                |
| `affinity`                                      | Affinity settings for pod assignment          | `{}`                                                                |
| `ingressClass.enabled`                          | Ingress class enabled                         | `true`                                                              |
| `ingressClass.isDefaultClass`                   | Ingress class is default class                | `true`                                                              |
| `metacontroller.enabled`                        | Metacontroller subchart enabled               | `false`                                                             |
| `metacontroller.*`                              | Metacontroller values                         | See [here](https://metacontroller.github.io/metacontroller/guide/helm-install.html) |
